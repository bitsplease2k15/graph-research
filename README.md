### Setup/Installation
* Clone/download the repo/folder to a location of your choice
* Open the terminal (Mac)/command-prompt (Windows) and type the following command:
```shell
pip3 install -r requirements.txt
```
* If above command fails try:
```shell
pip install -r requirements.txt
```
* If you use anaconda--add anaconda to system PATH or open anaconda-prompt and try:
```shell
conda install -r requirements.txt
```

### Experimental (Using Graph Projections to Reduce Dimensionality)
![outcome](./readme_imgs/projection.JPG)
